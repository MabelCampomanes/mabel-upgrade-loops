// Iteración #4: Probando For...in

const alien = {
    nombre: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}

for (const datos in alien) {
    console.log(datos, ': ' , alien[datos])
}

