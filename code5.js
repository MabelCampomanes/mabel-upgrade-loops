// Iteración #5: Probando For

const placesToTravel = [
    {
        id: 5, 
        name: 'Japan'
    }, 
    {
        id: 11, 
        name: 'Venecia'
    }, 
    {
        id: 23, 
        name: 'Murcia'
    }, 
    {
        id: 40, 
        name: 'Santander'
    }, 
    {
        id: 44, 
        name: 'Filipinas'
    }, 
    {
        id: 59, 
        name: 'Madagascar'
    }
    ]

for (i=0; i<placesToTravel.length; i++){
    let item = placesToTravel[i];

    if(item.id === 11){
        placesToTravel.splice(i,1);
    } else if (item.id === 40){
        placesToTravel.splice(i,1);
    } 
    console.log(placesToTravel)
}